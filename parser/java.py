from antlr4 import FileStream, CommonTokenStream, ParseTreeWalker
from antlr4.tree.Tree import ParseTreeListener

from parser._java.JavaLexer import JavaLexer
from parser._java.JavaParser import JavaParser


def parse(java_file: str, encoding="utf-8"):
    i = FileStream(java_file, encoding=encoding)
    lexer = JavaLexer(i)
    token_stream = CommonTokenStream(lexer)
    parser = JavaParser(token_stream)
    return parser.compilationUnit()


def has_unused_slf4j(java_file: str) -> bool:
    tree = parse(java_file)
    visitor_for_slf4j = AnnotationCollector(relevant=["Slf4j"])
    ParseTreeWalker().walk(visitor_for_slf4j, tree)
    has_slf4j = True if visitor_for_slf4j.collected else False
    if has_slf4j:
        visitor_for_used_logger = VariableCollector(relevant=["LOGGER"])
        ParseTreeWalker().walk(visitor_for_used_logger, tree)
        return False if visitor_for_used_logger.collected else True
    return False


class AnnotationCollector(ParseTreeListener):
    def __init__(self, relevant=None):
        self._relevant = relevant
        self.collected = None
        # we assume that it is not possible to have an annotation in an annotation, so a simple boolean value is fine.
        # if annotations in annotations were possible, we would have to maintain a stack.
        self._in_annotation: bool = False

    def enterAnnotation(self, ctx: JavaParser.AnnotationContext):
        self._in_annotation = True

    def exitAnnotation(self, ctx: JavaParser.AnnotationContext):
        self._in_annotation = False

    def enterQualifiedName(self, ctx: JavaParser.QualifiedNameContext):
        if not self._in_annotation:
            return
        qualifiedName = ctx.getText()
        if any(
            is_equal_qualified_name_or_simple_symbol(
                given=qualifiedName, expected=relevant
            )
            for relevant in self._relevant
        ):
            if self.collected is None:
                self.collected = []
            self.collected.append(ctx)


class VariableCollector(ParseTreeListener):
    def __init__(self, relevant=None):
        self._relevant = relevant
        self.collected = None

    def enterQualifiedName(self, ctx: JavaParser.QualifiedNameContext):
        qualifiedName = ctx.getText()
        if any(
            is_equal_qualified_name_or_simple_symbol(
                given=qualifiedName, expected=relevant
            )
            for relevant in self._relevant
        ):
            if self.collected is None:
                self.collected = []
            self.collected.append(ctx)

    def enterIdentifier(self, ctx: JavaParser.IdentifierContext):
        name = ctx.getText()
        if any(
            is_equal_qualified_name_or_simple_symbol(given=name, expected=relevant)
            for relevant in self._relevant
        ):
            if self.collected is None:
                self.collected = []
            self.collected.append(ctx)


def is_equal_qualified_name_or_simple_symbol(given, expected):
    if given == expected:
        return True
    if "." not in given and "." not in expected:
        return False
    # turn qualified names into simple names (e.g., "a.b.c" -> "c")
    given = given.rpartition(".")[-1]
    expected = expected.rpartition(".")[-1]
    return is_equal_qualified_name_or_simple_symbol(given, expected)
