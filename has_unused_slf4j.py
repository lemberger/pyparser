#!/usr/bin/env python3

import argparse
import sys
import multiprocessing

from tqdm import tqdm
from parser import java


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("file", nargs="+", help="Java file(s) to check")
    return parser.parse_args(argv)


def _print(s: str):
    print(s)


def _print_to_user(s: str):
    print(s, file=sys.stderr)


def map_used_to_None(java_file):
    if java.has_unused_slf4j(java_file):
        return java_file
    return None


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parse_args(argv)
    _print_to_user(f"Checking {len(args.file)} files")
    pool = multiprocessing.Pool()
    filter_used = pool.map(map_used_to_None, tqdm(args.file))
    with_unused = [e for e in filter_used if e is not None]
    if with_unused:
        for java_file in with_unused:
            _print(java_file)
        _print_to_user(f"❌ Found {len(with_unused)} files with unused Slf4j annotation")
    else:
        _print_to_user("🎉 No files with unused Slf4j!")


if __name__ == "__main__":
    sys.exit(main())
