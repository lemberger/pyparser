#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
MODULE_DIR="$SCRIPT_DIR/../parser/_java"

java -cp "$SCRIPT_DIR"/antlr4-4.12.0-complete.jar org.antlr.v4.Tool -Dlanguage=Python3 -visitor "$MODULE_DIR"/JavaLexer.g4 "$MODULE_DIR"/JavaParser.g4